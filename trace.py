import re
import os
import sys
import pandas as pd
import warnings
warnings.filterwarnings("ignore")

# Farihin(194639N)
# 14/02/2022

splitlist = []
col_list = ["Flow ID", "Src IP", "Src Port",
            "Dst IP", "Dst Port", "Protocol", "predicted(Label)"]


def benign(csv_file, pcap_file):
    try:
        split_tup = os.path.splitext(csv_file)
        split_tup2 = os.path.splitext(pcap_file)
        file_ext = split_tup[1]
        file_ext2 = split_tup2[1]
        if file_ext == '.csv' and file_ext2 == '.pcap':
            print("Processing...")
            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)

            cols = ['Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Protocol']
            newcol = ['-'.join(i) for i in zip(df['Src IP'], df['Src Port'],
                                               df['Dst IP'], df['Dst Port'], df['Protocol'].map(str))]
            new_flow = df.assign(NewFlowID=newcol).drop(cols, 1)
            directory = "Benign"

            benign_df = new_flow[df['predicted(Label)'] == 'BENIGN']
            newflow = benign_df['NewFlowID']

            originalflow = benign_df['Flow ID']

            flow_list = newflow.values.tolist()
            flow_list2 = originalflow.values.tolist()
            try:
                for flow in flow_list:
                    splitted = flow.split("-")
                    splitlist.append(splitted)
                    print("list of split increases", splitlist)

                    for data in splitlist:
                        os.system("tshark -r " + pcap_file + " -w " + 'Benign_' + flow +
                                  ".pcap -F pcap 'ip.src=={} && ip.dst=={} && tcp.srcport=={} && 	tcp.dstport=={}'".format(data[0], data[2], data[1], data[3]))

                        if not os.path.exists(directory):
                            os.makedirs(directory)
                            os.system("sudo mv Benign_?* " + directory)
                        else:
                            os.system("sudo mv Benign_?* " + directory)

                print("..................................")
                print("Completed.")
                path = os.getcwd() + "/" + directory
                list_dir = []
                list_dir = os.listdir(path)
                count = 0
                for file in list_dir:
                    if file.endswith('.pcap'):
                        count += 1
                print(count, "pcap files has been added into",
                      directory, "folder.")

            except Exception as e:
                print(e)
                print("Something went wrong.")

        else:
            print("Invalid file format.")
    except:
        print("Something went wrong.")


def bot(csv_file, pcap_file):
    try:
        split_tup = os.path.splitext(csv_file)
        split_tup2 = os.path.splitext(pcap_file)
        file_ext = split_tup[1]
        file_ext2 = split_tup2[1]
        if file_ext == '.csv' and file_ext2 == '.pcap':
            print("Processing...")
            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)

            cols = ['Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Protocol']
            newcol = ['-'.join(i) for i in zip(df['Src IP'], df['Src Port'],
                                               df['Dst IP'], df['Dst Port'], df['Protocol'].map(str))]
            new_flow = df.assign(NewFlowID=newcol).drop(cols, 1)
            directory = "Bot"

            bot_df = new_flow[df['predicted(Label)'] == 'Bot']
            newflow = bot_df['NewFlowID']

            originalflow = bot_df['Flow ID']

            flow_list = newflow.values.tolist()
            flow_list2 = originalflow.values.tolist()
            try:
                for flow in flow_list:
                    splitted = flow.split("-")
                    splitlist.append(splitted)
                    print("list of split increases", splitlist)

                    for data in splitlist:
                        os.system("tshark -r " + pcap_file + " -w " + 'Bot_' + flow +
                                  ".pcap -F pcap 'ip.src=={} && ip.dst=={} && tcp.srcport=={} && 	tcp.dstport=={}'".format(data[0], data[2], data[1], data[3]))

                        if not os.path.exists(directory):
                            os.makedirs(directory)
                            os.system("sudo mv Bot_?* " + directory)
                        else:
                            os.system("sudo mv Bot_?* " + directory)

                print("..................................")
                print("Completed.")
                path = os.getcwd() + "/" + directory
                list_dir = []
                list_dir = os.listdir(path)
                count = 0
                for file in list_dir:
                    if file.endswith('.pcap'):
                        count += 1
                print(count, "pcap files has been added into",
                      directory, "folder.")

            except Exception as e:
                print(e)
                print("Something went wrong.")

        else:
            print("Invalid file format.")
    except:
        print("Something went wrong.")


def ddos(csv_file, pcap_file):
    try:
        split_tup = os.path.splitext(csv_file)
        split_tup2 = os.path.splitext(pcap_file)
        file_ext = split_tup[1]
        file_ext2 = split_tup2[1]
        if file_ext == '.csv' and file_ext2 == '.pcap':
            print("Processing...")
            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)

            cols = ['Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Protocol']
            newcol = ['-'.join(i) for i in zip(df['Src IP'], df['Src Port'],
                                               df['Dst IP'], df['Dst Port'], df['Protocol'].map(str))]
            new_flow = df.assign(NewFlowID=newcol).drop(cols, 1)
            directory = "DDOS"

            ddos_df = new_flow[df['predicted(Label)'] == 'DDoS']
            newflow = ddos_df['NewFlowID']

            originalflow = ddos_df['Flow ID']

            flow_list = newflow.values.tolist()
            flow_list2 = originalflow.values.tolist()
            try:
                for flow in flow_list:
                    splitted = flow.split("-")
                    splitlist.append(splitted)
                    print("list of split increases", splitlist)

                    for data in splitlist:
                        os.system("tshark -r " + pcap_file + " -w " + 'DDOS_' + flow +
                                  ".pcap -F pcap 'ip.src=={} && ip.dst=={} && tcp.srcport=={} && 	tcp.dstport=={}'".format(data[0], data[2], data[1], data[3]))

                        if not os.path.exists(directory):
                            os.makedirs(directory)
                            os.system("sudo mv DDOS_?* " + directory)
                        else:
                            os.system("sudo mv DDOS_?* " + directory)

                print("..................................")
                print("Completed.")
                path = os.getcwd() + "/" + directory
                list_dir = []
                list_dir = os.listdir(path)
                count = 0
                for file in list_dir:
                    if file.endswith('.pcap'):
                        count += 1
                print(count, "pcap files has been added into",
                      directory, "folder.")

            except Exception as e:
                print(e)
                print("Something went wrong.")

        else:
            print("Invalid file format.")
    except:
        print("Something went wrong.")


def infiltration(csv_file, pcap_file):
    try:
        split_tup = os.path.splitext(csv_file)
        split_tup2 = os.path.splitext(pcap_file)
        file_ext = split_tup[1]
        file_ext2 = split_tup2[1]
        if file_ext == '.csv' and file_ext2 == '.pcap':
            print("Processing...")
            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)

            cols = ['Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Protocol']
            newcol = ['-'.join(i) for i in zip(df['Src IP'], df['Src Port'],
                                               df['Dst IP'], df['Dst Port'], df['Protocol'].map(str))]
            new_flow = df.assign(NewFlowID=newcol).drop(cols, 1)
            directory = "Infiltration"

            inf_df = new_flow[df['predicted(Label)'] == 'Infilteration']
            newflow = inf_df['NewFlowID']

            originalflow = inf_df['Flow ID']
            flow_list = newflow.values.tolist()
            flow_list2 = originalflow.values.tolist()

            try:
                for flow in flow_list:

                    splitted = flow.split("-")

                    splitlist.append(splitted)
                    print("list of split increases", splitlist)

                    for data in splitlist:
                        os.system("tshark -r " + pcap_file + " -w " + 'Infiltration_' + flow +
                                  ".pcap -F pcap 'ip.src=={} && ip.dst=={} && tcp.srcport=={} && 	tcp.dstport=={}'".format(data[0], data[2], data[1], data[3]))

                        if not os.path.exists(directory):
                            os.makedirs(directory)
                            os.system("sudo mv Infiltration_?* " + directory)
                        else:
                            os.system("sudo mv Infiltration_?* " + directory)

                print("..................................")
                print("Completed.")
                path = os.getcwd() + "/" + directory
                list_dir = []
                list_dir = os.listdir(path)
                count = 0
                for file in list_dir:
                    if file.endswith('.pcap'):
                        count += 1
                print(count, "pcap files has been added into",
                      directory, "folder.")

            except Exception as e:
                print(e)
                print("Something went wrong.")

        else:
            print("Invalid file format.")
    except:
        print("Something went wrong.")


def wba(csv_file, pcap_file):
    try:
        split_tup = os.path.splitext(csv_file)
        split_tup2 = os.path.splitext(pcap_file)
        file_ext = split_tup[1]
        file_ext2 = split_tup2[1]
        if file_ext == '.csv' and file_ext2 == '.pcap':
            print("Processing...")

            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)

            cols = ['Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Protocol']
            newcol = ['-'.join(i) for i in zip(df['Src IP'], df['Src Port'],
                                               df['Dst IP'], df['Dst Port'], df['Protocol'].map(str))]
            new_flow = df.assign(NewFlowID=newcol).drop(cols, 1)
            directory = "WebAttack"

            wba_df = new_flow[df['predicted(Label)'] == 'Web Attack']
            newflow = wba_df['NewFlowID']

            originalflow = wba_df['Flow ID']

            flow_list = newflow.values.tolist()
            flow_list2 = originalflow.values.tolist()
            try:
                for flow in flow_list:
                    splitted = flow.split("-")
                    splitlist.append(splitted)
                    print("list of split increases", splitlist)

                    for data in splitlist:
                        os.system("tshark -r " + pcap_file + " -w " + 'WebAttack_' + flow +
                                  ".pcap -F pcap 'ip.src=={} && ip.dst=={} && tcp.srcport=={} && 	tcp.dstport=={}'".format(data[0], data[2], data[1], data[3]))

                        if not os.path.exists(directory):
                            os.makedirs(directory)
                            os.system("sudo mv WebAttack_?* " + directory)
                        else:
                            os.system("sudo mv WebAttack_?* " + directory)

                print("..................................")
                print("Completed.")
                path = os.getcwd() + "/" + directory
                list_dir = []
                list_dir = os.listdir(path)
                count = 0
                for file in list_dir:
                    if file.endswith('.pcap'):
                        count += 1
                print(count, "pcap files has been added into",
                      directory, "folder.")

            except Exception as e:
                print(e)
                print("Something went wrong.")
        else:
            print("Invalid file format.")
    except:
        print("Something went wrong.")


def help():
    print("Farihin-194639N-14-02-2022")
    print("----------------------------------------------")
    print("Trace flow identity of output data with original dataset to generate smaller ")
    print("network traffic flows according to label specified by the user.")
    print("")
    print("Usage: trace.py {ddos/wba/bot/benign/infiltration} ... [<infile1>] [<infile2>] ")
    print("       trace.py search [<label>]")
    print("       trace.py help")
    print("")

    print("Description: Output.pcap files will be stored in the respective directory ")
    print("             according to the chosen labels.")
    print("")
    print("Example: ../WebAttack/WebAttack_{output-flow-identity}.pcap")
    
    print("")
    print("Input file format: ")
    print(".csv    ........  [<infile1>]  Output dataset with model applied")
    print(".pcap   ........  [<infile2>]  Original dataset file")
    print("")

    print("Available commands:")
    print("")
    print("benign            [<infile1>] [<infile2>]")
    print("bot               [<infile1>] [<infile2>]")
    print("ddos              [<infile1>] [<infile2>]")
    print("wba (web attack)  [<infile1>] [<infile2>]")
    print("infiltration      [<infile1>] [<infile2>]")
    print("")
    print("Example: trace.py bot dataset.csv dataset.pcap")
    print("")
    print("search            [<infile1>] [<label>]  ")
    print("")
    print("Example: trace.py check dataset.csv Bot")
    print("")


def search(csv_file, attack):
    try:
        split_tup = os.path.splitext(csv_file)
        file_ext = split_tup[1]
        if file_ext == '.csv':
            print("Filtering in process...")
            col_list = ["predicted(Label)"]
            df = pd.read_csv(csv_file, usecols=col_list)
            df = df.astype(str)
            attack_df = df[df['predicted(Label)'].str.contains(
                r'(?:\s|^)' + attack + r'(?:\s|$)', flags=re.IGNORECASE) == True]
            if attack_df.empty:
                print("------------------------------")
                print(attack, "not found.")
            else:
                print("------------------------------")
                print(attack_df)
                print("------------------------------")
                print(attack, "found.")

        else:
            print("Invalid file format. Must be in .CSV format.")
    except Exception as e:
        print(e)


if __name__ == "__main__":
    args = sys.argv
    globals()[args[1]](*args[2:])
